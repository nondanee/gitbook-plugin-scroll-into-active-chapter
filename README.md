# Scroll Into Active Chapter for GitBook

### How to use it?

Add it to your `book.json` configuration:

```
{
    "plugins": ["scroll-into-active-chapter"]
}
```

Install your plugins using:

```
$ gitbook install
```